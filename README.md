# Finance Manager (Client)

A simple to use web app for all your finance

## Author

### [Harsh Patel](https://gitlab.com/harsh2124)

## Installing / Getting started

A quick introduction of the setup you need to follow to get the app up & running.

```bash
git clone https://harsh-coderc@bitbucket.org/harsh-coderc/finance-manager-client.git
cd finance-manager-client
yarn install
yarn start
```

## Licensing

The code in this project is licensed under MIT license.
