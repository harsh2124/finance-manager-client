/**
 * @author Harsh Patel
 */

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import './scss/index.scss';
import store from './utils/store';
import Wrapper from './utils/Wrapper';

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <Wrapper />
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);
