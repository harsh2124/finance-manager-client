import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import About from '../components/About';
import Accounts from '../components/Accounts';
import Budget from '../components/Budget';
import Home from '../components/Home';
import Profile from '../components/Profile';
import Transactions from '../components/Transactions';

const AuthenticatedRoutes = () => {
    return (
        <Switch>
            <Route exact path="/about" component={About} />
            <Route exact path="/accounts" component={Accounts} />
            <Route exact path="/budget" component={Budget} />
            <Route exact path="/" component={Home} />
            <Route exact path="/profile" component={Profile} />
            <Route exact path="/transactions" component={Transactions} />
            <Redirect to="/" />
        </Switch>
    );
};

export default AuthenticatedRoutes;
