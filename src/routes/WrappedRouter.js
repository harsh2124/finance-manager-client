import React from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import AuthenticatedRoutes from './AuthenticatedRoutes';
import UnauthenticatedRoutes from './UnauthenticatedRoutes';

const WrappedRouter = ({ user }) => {
    return (
        <Router>
            {user.isUserAuthenticated ? <AuthenticatedRoutes /> : <UnauthenticatedRoutes />}
        </Router>
    );
};

export default connect(state => {
    return {
        user: state.user,
    };
})(WrappedRouter);
