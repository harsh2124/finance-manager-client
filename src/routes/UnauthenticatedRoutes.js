import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import AuthForm from '../components/AuthForm';
import GuestLandingPage from '../components/GuestLandingPage';

const UnauthenticatedRoutes = () => {
    return (
        <Switch>
            <Route exact path="/" component={GuestLandingPage} />
            <Route exact path="/signin" render={props => <AuthForm {...props} routeType={0} />} />
            <Route exact path="/signup" render={props => <AuthForm {...props} routeType={1} />} />
            <Redirect to="/" />
        </Switch>
    );
};

export default UnauthenticatedRoutes;
