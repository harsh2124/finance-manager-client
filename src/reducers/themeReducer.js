/**
 * @author Harsh Patel
 */

const initialState = {
    themeColor: '#343a40',
};

const theme = (state = initialState, action) => {
    switch (action.type) {
        default:
            return state;
    }
};

export default theme;
