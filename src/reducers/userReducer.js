/**
 * @author Harsh Patel
 */

const initialState = {
    isUserAuthenticated: true,
};

const user = (state = initialState, action) => {
    switch (action.type) {
        default:
            return state;
    }
};

export default user;
