/**
 * @author Harsh Patel
 */

import { combineReducers } from 'redux';
import user from './userReducer';
import theme from './themeReducer';

const root = combineReducers({
    user,
    theme,
});

export default root;
