/**
 * @author Harsh Patel
 */

import MomentUtils from '@date-io/moment';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import React from 'react';
import { connect } from 'react-redux';
import WrappedRouter from '../routes/WrappedRouter';

const Wrapper = props => {
    const theme = createMuiTheme({
        palette: {
            primary: {
                main: props.theme.themeColor,
            },
        },
    });

    return (
        <MuiThemeProvider theme={theme}>
            <MuiPickersUtilsProvider utils={MomentUtils}>
                <WrappedRouter />
            </MuiPickersUtilsProvider>
        </MuiThemeProvider>
    );
};

export default connect(state => {
    return {
        theme: state.theme,
    };
})(Wrapper);
