/**
 * @author Harsh Patel
 */

import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import logger from 'redux-logger';
import root from '../reducers/rootReducer';

const store = createStore(root, composeWithDevTools(applyMiddleware(logger)));

export default store;
