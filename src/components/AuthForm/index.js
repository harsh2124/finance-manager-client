import React from 'react';

const AuthForm = ({ routeType }) => {
    return (
        <div>
            <h1>{routeType ? 'Sign Up' : 'Sign In'}</h1>
        </div>
    );
};

export default AuthForm;
